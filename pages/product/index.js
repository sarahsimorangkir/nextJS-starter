import Link from "next/link";

function Product({ productId = 100 }) {
  return (
    <>
      <h2>
        <Link href="/product">Product id 12</Link>
      </h2>
      <h2>
        <Link href={`/product/${productId}`}>Produk {productId}</Link>
      </h2>
    </>
  );
}
export default Product;
