import Link from "next/link";
import React from "react";
import styles from "./posts.module.css";

function PostList({ posts }) {
  return (
    <>
      <h1>Test Postlist</h1>
      {posts.map((post) => {
        return (
          <div className={styles.PostList} key={post.id}>
            <Link href={`posts/${post.id}`} passHref>
              <p>{post.title}</p>
            </Link>
          </div>
        );
      })}
    </>
  );
}
export default PostList;

export async function getStaticProps() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = await response.json();
  console.log(data);

  return {
    props: {
      posts: data.slice(0,6),
    },
  };
}
