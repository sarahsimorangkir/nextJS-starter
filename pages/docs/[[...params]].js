import { useRouter } from "next/router";

function Doc() {
  const router = useRouter();
  const { params = [] } = router.query;
  console.log(params);

  //condition for params
  if (params.length === 2) {
    return (
      <div>
        <h1>
          This page consist of {params[0]} and review {params[1]}
        </h1>
      </div>
    );
  } else if (params.length === 1) {
    return (
      <div>
        <h1>This page consist of {params[0]}</h1>
      </div>
    );
  }

  return (
    <div>
      <h1>This is the document page</h1>
    </div>
  );
}

export default Doc;

//double array on naming convention to show root parameters
