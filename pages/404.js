import Image from "next/image";
import Error from "@/public/illustration-error-404.svg";
//comments example
/**
 * customize the 404 error page
 * @param config  config object
 * @param onGetInstance function that will be called when the SDK instance has successfully loaded
 * @param sourceUrl source URL of..
 * @returns {JSX.Element}
 * @constructor
 */

function PageNotFound() {
    //style for image
  const customStyle = {
    margin: "0 auto",
    borderRadius: "14px",
    boxShadow: "0px 0px 10px 16px rgba(0, 0, 0, 0.1)",
    display: "block",
  };


  return (
    <div>
      <Image
        alt="Error picture"
        width={200}
        height={200}
        src={Error}
        style={customStyle}
      />
      <h2 style={{textAlign:"center"}}>Sorry, This Page is Not Found</h2>
    </div>
  );
}
export default PageNotFound;
