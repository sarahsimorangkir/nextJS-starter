import Link from "next/link";
import { useRouter } from "next/router";

function Home() {
  const router = useRouter();

  //The push() method adds a new entry to the history stack, while the replace() method replaces the current entry on the history stack.
  const handleOnClick = () => {
    console.log("Placing ur order noww");
    router.replace("/product");
  };

  return (
    <>
      <h1>Home Pagee</h1>
      <h1> - Product</h1>
      <h1>
        <Link href="/users/users"> - User</Link>
      </h1>
      <h1>
        <Link href="/posts"> - Post List</Link>
      </h1>
      <div>
        <button onClick={handleOnClick}>Cart</button>
      </div>
    </>
  );
}
export default Home


