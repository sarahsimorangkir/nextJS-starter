import User from "@/component/user";
import React from "react";
import styles from './user.module.css';

function Users({ users }) {
  return (
    <>
      <h3>User Pre-rendering:</h3>
      {users.map((user) => {
        return (
          <div className={styles.User} key={user.id}>
            <User user={user} />
            {/* <tr>
              <td>{user.name}</td>
              <td>{user.email}</td>
            </tr> */}
          </div>
        );
      })}
    </>
  );
}

export default Users;

export async function getStaticProps() {
  const response = await fetch("https://jsonplaceholder.typicode.com/users");
  const data = await response.json();
  console.log(data);

  return {
    props: {
      users: data,
    },
  };
}
